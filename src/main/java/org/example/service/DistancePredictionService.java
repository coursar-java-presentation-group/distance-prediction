package org.example.service;

public class DistancePredictionService {
    public int predict(double consumption, int volume) {
        int reserve = 5;
        int prediction = (int) (volume / consumption * 100) - reserve;
        // early exit
        if (prediction < 0) {
           return 0;
        }
        return prediction;
    }
}
