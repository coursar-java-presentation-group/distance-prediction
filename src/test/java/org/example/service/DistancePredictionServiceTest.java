package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistancePredictionServiceTest {
    @Test
    void shouldPredictGreaterThanReserve() {
        // A(rrange)
        DistancePredictionService service = new DistancePredictionService();
        double consumption = 7.9;
        int volume = 20;
        int expected = 248;

        // A(ct)
        int actual = service.predict(consumption, volume);

        // A(ssert)
        assertEquals(expected, actual);
    }

    @Test
    void shouldPredictLessThanReserve() {
        // A(rrange)
        DistancePredictionService service = new DistancePredictionService();
        double consumption = 25.4;
        int volume = 1;
        int expected = 0;

        // A(ct)
        int actual = service.predict(consumption, volume);

        // A(ssert)
        assertEquals(expected, actual);
    }

    @Test
    void shouldPredictEqualToReserve() {
        // A(rrange)
        DistancePredictionService service = new DistancePredictionService();
        double consumption = 25.4;
        int volume = 0;
        int expected = 0;

        // A(ct)
        int actual = service.predict(consumption, volume);

        // A(ssert)
        assertEquals(expected, actual);
    }
}
